import { Meteor } from 'meteor/meteor';
import { Tabular } from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
 import { Tracker } from 'meteor/tracker'

import { dbNews } from "./data";

Tracker.autorun(() => {
  Meteor.subscribe('dbNews');
});

TabularTables = {};
// Meteor.isClient && Template.registerHelper('TabularTables', dbNews);
Template.registerHelper('TabularTables', {
  myTable: function () {
    return TabularTables.dbNews;
  }
});

TabularTables.dbNews = new Tabular.Table({
  name: "dbNews",
  collection: dbNews,
  pub:"dbNews",
  columns: [
    {data: "name", title: "Name", className: "nameColumn" },
    {data: "phone", title: "Phone"},
    {data: "createAt", title: "Date Time"},
    {
      tmpl: Meteor.isClient && Template.bookCheckOutCell
    }
  ]
});

//   ],
// regex: true,
// paging: true,
// search: {
//   smart: true,
// },
// searchDelay: 2000,
// });