import { Meteor } from 'meteor/meteor';
 import { dbNews } from '../lib/data.js';

Meteor.publish('dbNews', function () {
  return dbNews.find({});
});
